from pymongo import MongoClient
import utils
import os

if __name__ == '__main__':
    client = MongoClient("localhost", 27017)
    db = client.vaf
    cursor = db.DataContentImage
    key = 'b64'
    for doc in cursor.find({ key: {'$exists': True }}):
        image = utils.getb64(doc[key])
        image = image[0]
        fext = image[0]
        ib64 = image[1]
        fname = utils.write( ib64, fext )
        utils.convert(fname)
        
        obj_img = {
            key : {
                    'desktop' : {
                        'fileName' : os.path.basename(fname),
                        'result': fname,
                        'preview': fname,
                        'name': fname,
                        'resultWEBP': 'data/images/' + utils.md5(ib64) + '.webp'
                    }
                }
            }
        cursor.find_one_and_update({"_id": doc['_id']}, {"$set": obj_img})
        #print obj_img
            
        
