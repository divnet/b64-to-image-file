#!/usr/bin/env python
import sys
import os
import base64
import uuid
import hashlib
import re

    # arglen = len(sys.argv)
    # if arglen > 1:
    #     b64file = open(sys.argv[1], 'rb').read()
    #     imgData = base64.b64decode(b64file)
    #     file_name = os.path.splitext(sys.argv[1])
    #     fname = file_name[0]
    #     fext = '.png'

    #     imgFile = open(fname + fext, 'wb')
    #     imgFile.write(imgData)
    #     print('done')
    # else:
    #     print('No file specified!')

def decodeb64(b64Data):
    return base64.b64decode(b64Data)

def random_name():
    return str(uuid.uuid4())

def getb64(string):
    match = re.findall(r"data:image\/([a-zA-Z]*);base64,([^\"]*)", string)
    return match

def write(data, fext = 'jpg'):
    dirbase = 'data/images/'
    fname = dirbase + md5(data) + '.' + fext
    f = open(fname, 'wb')
    f.write(decodeb64(data))
    print "saved %s" % fname
    return fname

def md5(string):
    m = hashlib.md5()
    m.update(string)
    return m.hexdigest()


def convert(imagePath, quality = 100):
    from PIL import Image
    path = os.path.basename(imagePath)
    name, ext = path.split(".")
    outputPath = "data/images/" + name + '.webp'
    
    im = Image.open(imagePath)
    im.save(outputPath,'webp', quality = quality)